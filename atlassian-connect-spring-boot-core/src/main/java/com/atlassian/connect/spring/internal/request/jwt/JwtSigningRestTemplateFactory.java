package com.atlassian.connect.spring.internal.request.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * A provider of a {@link RestTemplate} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
@Component
public class JwtSigningRestTemplateFactory {

    private RestTemplate restTemplate;

    @Autowired
    public JwtSigningRestTemplateFactory(RestTemplateBuilder restTemplateBuilder,
            JwtSigningClientHttpRequestInterceptor requestInterceptor) {
        restTemplate = restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }

    @Bean
    public RestTemplate getJwtRestTemplate() {
        return restTemplate;
    }
}
