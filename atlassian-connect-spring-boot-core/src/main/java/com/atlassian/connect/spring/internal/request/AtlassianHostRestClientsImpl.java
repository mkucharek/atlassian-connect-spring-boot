package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.jwt.JwtGenerator;
import com.atlassian.connect.spring.internal.request.jwt.JwtSigningRestTemplateFactory;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2RestTemplateFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class AtlassianHostRestClientsImpl implements AtlassianHostRestClients {

    private JwtSigningRestTemplateFactory jwtSigningRestTemplateFactory;

    private JwtGenerator jwtGenerator;

    private OAuth2RestTemplateFactory oauth2RestTemplateFactory;

    private AtlassianConnectSecurityContextHelper securityContextHelper;

    @Autowired
    public AtlassianHostRestClientsImpl(JwtSigningRestTemplateFactory jwtSigningRestTemplateFactory,
            JwtGenerator jwtGenerator,
            OAuth2RestTemplateFactory oauth2RestTemplateFactory,
            AtlassianConnectSecurityContextHelper securityContextHelper) {
        this.jwtSigningRestTemplateFactory = jwtSigningRestTemplateFactory;
        this.jwtGenerator = jwtGenerator;
        this.oauth2RestTemplateFactory = oauth2RestTemplateFactory;
        this.securityContextHelper = securityContextHelper;
    }

    @Override
    public RestTemplate authenticatedAsAddon() {
        return jwtSigningRestTemplateFactory.getJwtRestTemplate();
    }

    @Override
    public String createJwt(HttpMethod method, URI uri) {
        return jwtGenerator.createJwtToken(method, uri);
    }

    @Override
    public OAuth2RestTemplate authenticatedAsHostActor() {
        AtlassianHostUser hostUser = securityContextHelper.getHostUserFromSecurityContext()
                .orElseThrow(() -> new IllegalStateException("Was asked to authenticate the rest client as " +
                        "the current acting user on the host, " +
                        "but none could be inferred from the current request"));
        return authenticatedAs(hostUser);
    }

    @Override
    public OAuth2RestTemplate authenticatedAs(AtlassianHostUser hostUser) {
        return oauth2RestTemplateFactory.getOAuth2RestTemplate(hostUser);
    }
}
