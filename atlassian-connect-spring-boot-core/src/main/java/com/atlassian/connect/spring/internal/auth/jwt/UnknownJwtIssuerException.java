package com.atlassian.connect.spring.internal.auth.jwt;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UnknownJwtIssuerException extends UsernameNotFoundException {

    private final String issuer;

    public UnknownJwtIssuerException(String issuer) {
        this(issuer, null);
    }

    public UnknownJwtIssuerException(String issuer, Throwable throwable) {
        super("Could not find an installed host for the provided client key: " + issuer, throwable);
        this.issuer = issuer;
    }

    public String getIssuer() {
        return issuer;
    }
}
