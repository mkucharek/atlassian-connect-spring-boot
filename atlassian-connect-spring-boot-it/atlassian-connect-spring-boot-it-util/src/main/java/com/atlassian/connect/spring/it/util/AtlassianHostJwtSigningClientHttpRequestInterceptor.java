package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.atlassian.connect.spring.internal.request.jwt.JwtQueryHashGenerator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Optional;

public class AtlassianHostJwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private final String clientKey;
    private final String sharedSecret;
    private final Optional<String> optionalSubject;
    private final JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(AtlassianHost host, Optional<String> optionalSubject) {
        this(host.getClientKey(), host.getSharedSecret(), optionalSubject);
    }

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(String clientKey, String sharedSecret, Optional<String> optionalSubject) {
        super("some-version");
        this.clientKey = clientKey;
        this.sharedSecret = sharedSecret;
        this.optionalSubject = optionalSubject;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(AtlassianHosts.BASE_URL);
        return Optional.of(host);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", createJwt(request.getMethod(), request.getURI())));
        return request;
    }

    public String createJwt(HttpMethod method, URI uri) {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(
                method, uri, AtlassianHostUriResolver.getBaseUrl(uri));
        String queryHash = queryHashGenerator.computeCanonicalRequestHash(canonicalHttpRequest);
        JwtBuilder jwtBuilder = new JwtBuilder()
                .issuer(clientKey)
                .queryHash(queryHash)
                .signature(sharedSecret);
        optionalSubject.ifPresent(jwtBuilder::subject);
        return jwtBuilder.build();
    }
}
