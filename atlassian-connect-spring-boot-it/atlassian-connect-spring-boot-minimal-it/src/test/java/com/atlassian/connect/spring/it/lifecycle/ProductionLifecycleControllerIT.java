package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleEventMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@ActiveProfiles("production")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductionLifecycleControllerIT extends BaseApplicationIT {

    @Test
    public void shouldRejectInstallFromUnknownHostInProdMode() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().build();
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        ResponseEntity<String> response = restTemplate.postForEntity(getServerAddress() + "/installed",
                createLifecycleEventMap("installed"), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }
}
